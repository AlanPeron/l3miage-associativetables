package rattrapages2022;

import impl.Set;

public class Main {
	static Monument a = new Monument("type1", "monumentA", 10000, "addressA", "dateA");
	static Monument b = new Monument("type1", "monumentB", 10245, "addressB", "dateB");
	static Monument c = new Monument("type5", "monumentC", 36251, "addressC", "dateC");
	static Monument d = new Monument("type1", "monumentD", 25583, "addressD", "dateD");
	static Monument e = new Monument("type2", "monumentE", 10000, "addressE", "dateE");
	static Monument f = new Monument("type2", "monumentF", 25622, "addressF", "dateF");
	static Monument g = new Monument("type1", "monumentG", 25250, "addressG", "dateG");
	static Monument h = new Monument("type2", "monumentH", 25250, "addressH", "dateH");
	static Monument i = new Monument("type3", "monumentI", 10000, "addressI", "dateI");
	static Monument j = new Monument("type4", "monumentJ", 25632, "addressJ", "dateJ");
	static Monument k = new Monument("type1", "monumentK", 10450, "addressK", "dateK");
	

	public static void main(String[] args) {
		Set<Monument> dpt10 = new Set<>(a.getClass());
		dpt10.add(a);
		dpt10.add(b);
		dpt10.add(e);
		dpt10.add(i);
		dpt10.add(k);
		Set<Monument> dpt36 = new Set<>(a.getClass());
		dpt36.add(c);
		Set<Monument> dpt25 = new Set<>(a.getClass());
		dpt25.add(d);
		dpt25.add(f);
		dpt25.add(g);
		dpt25.add(h);
		dpt25.add(j);

		Merimee merimee = new Merimee();
		merimee.addValue((short) 10, dpt10);
		merimee.addValue((short) 36, dpt36);
		merimee.addValue((short) 25, dpt25);
		merimee.addValue((short) 50, new Set<>(a.getClass()));
		
		//Use your functions here
		System.out.println(merimee.toString());
		merimee.belongs(a);
	}
}
