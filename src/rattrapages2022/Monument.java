package rattrapages2022;

public class Monument {
	public String type;
	public String name;
	public int commune;
	public String address;
	public String date;
	
	public Monument(String type, String name, int commune, String address, String date) {
		this.type = type;
		this.name = name;
		this.commune = commune;
		this.address = address;
		this.date = date;
	}
	
	public String toString() {
		return "( type: " + type + ", " + "name: " + name + ", " + "commune: " + commune + ", " + "address: " + address + ", " + "date: " + date + " )";
	}
	
	public boolean equals(Monument monument) {
		return this.name == monument.name && this.commune == monument.commune;
	}
}
