package cc2021;

import impl.Set;

public class Main {
	static Collectable a = new Collectable("type1", "producer1", "a", "country1", 1.0);
	static Collectable b = new Collectable("type1", "producer1", "b", "country2", 20.0);
	static Collectable c = new Collectable("type5", "producer1", "c", "country1", 50.0);
	static Collectable d = new Collectable("type1", "producer2", "d", "country1", 505.0);
	static Collectable e = new Collectable("type2", "producer2", "e", "country1", 50.0);
	static Collectable f = new Collectable("type2", "producer2", "f", "country2", 5.0);
	static Collectable g = new Collectable("type1", "producer3", "g", "country1", 1.0);
	static Collectable h = new Collectable("type2", "producer3", "h", "country1", 0.5);
	static Collectable i = new Collectable("type3", "producer3", "i", "country1", 55.0);
	static Collectable j = new Collectable("type4", "producer3", "j", "country1", 51625.0);
	static Collectable k = new Collectable("type1", "producer4", "k", "country1", 5511.0);
	

	public static void main(String[] args) {
		Set<Collectable> type1Producer1 = new Set<>(a.getClass());
		type1Producer1.add(a);
		type1Producer1.add(b);
		Set<Collectable> type1Producer2 = new Set<>(a.getClass());
		type1Producer2.add(d);
		Set<Collectable> type1Producer3 = new Set<>(a.getClass());
		type1Producer3.add(g);
		Set<Collectable> type1Producer4 = new Set<>(a.getClass());
		type1Producer4.add(k);
		Set<Collectable> type5Producer1 = new Set<>(a.getClass());
		type5Producer1.add(c);
		Set<Collectable> type2Producer2 = new Set<>(a.getClass());
		type2Producer2.add(e);
		type2Producer2.add(f);
		Set<Collectable> type2Producer3 = new Set<>(a.getClass());
		type2Producer3.add(h);
		Set<Collectable> type3Producer3 = new Set<>(a.getClass());
		type3Producer3.add(i);
		Set<Collectable> type4Producer3 = new Set<>(a.getClass());
		type4Producer3.add(j);

		SubCollection subCollection1 = new SubCollection();
		subCollection1.addValue("producer1", type1Producer1);
		subCollection1.addValue("producer2", type1Producer2);
		subCollection1.addValue("producer3", type1Producer3);
		subCollection1.addValue("producer4", type1Producer4);
		SubCollection subCollection2 = new SubCollection();
		subCollection2.addValue("producer1", type5Producer1);
		SubCollection subCollection3 = new SubCollection();
		subCollection3.addValue("producer2", type2Producer2);
		subCollection3.addValue("producer3", type2Producer3);
		SubCollection subCollection4 = new SubCollection();
		subCollection4.addValue("producer3", type4Producer3);

		Collection collection = new Collection();
		collection.addValue("type1", subCollection1);
		collection.addValue("type2", subCollection2);
		collection.addValue("type3", subCollection3);
		collection.addValue("type4", subCollection4);
		
		//Use your functions here
		System.out.println(collection.toString());
		collection.sale("type1", "country1", 1.0);
		
	}
}
