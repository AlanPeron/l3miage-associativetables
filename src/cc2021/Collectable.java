package cc2021;

public class Collectable {
	public String type; // type d�objet
	public String producer; //fabricant
	public String name; // nom de l�objet
	public String country; // pays d�origine
	public double catalogPrice; //pric de catalogue
	
	public Collectable(String type, String producer, String name, String country, double catalogPrice) {
		this.type = type;
		this.producer = producer;
		this.name = name;
		this.country = country;
		this.catalogPrice = catalogPrice;
	}
	
	public String toString() {
		return "( type: " + type + ", " + "producer: " + producer + ", " + "name: " + name + ", " + "country: " + country + ", " + "catalogPrice: " + catalogPrice + " )";
	}
}
