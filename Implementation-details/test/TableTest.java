package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import impl.Table;
import interfaces.Iterator;
import interfaces.ITable;


class TableTest {
	ITable<Integer, String> table = new Table<Integer, String>();
	
	String JANE = "Jane SHEPARD";
	String GARRUS = "Garrus VAKARIAN";
	
	@Test
	public void addValueShouldAddValue() {
		table.addValue(1, JANE);

		assertEquals(JANE, table.getValue(1));
	}
	
	@Test
	public void addValueShouldNotReplaceValue() {
		table.addValue(1, JANE);
		table.addValue(1, GARRUS);
		table.addValue(2, GARRUS);

		assertEquals(JANE, table.getValue(1));
		assertEquals(GARRUS, table.getValue(2));
	}
	
	@Test
	public void containsShouldReturnTrueIfTableContainsKey() {
		table.addValue(1, JANE);

		assertEquals(true, table.contains(1));
	}
	
	@Test
	public void containsShouldReturnFalseIfTableDoesNotContainKey() {
		table.addValue(1, JANE);

		assertEquals(false, table.contains(2));
	}
	
	@Test
	public void isEmptyShouldReturnTrueIfTableIsEmpty() {
		assertEquals(true, table.isEmpty());
	}
	
	@Test
	public void isEmptyShouldReturnFalseIfTableIsNotEmpty() {
		table.addValue(1, JANE);

		assertEquals(false, table.isEmpty());
	}
	
	@Test
	public void clearShouldClearTheMap() {
		table.addValue(1, JANE);
		
		table.clear();

		assertEquals(true, table.isEmpty());
	}
	
	@Test
	public void modifyValueShouldChangeTheValueIfItExists() {
		table.addValue(1, JANE);
		
		String exValue = table.modifyValue(1, GARRUS);

		assertEquals(GARRUS, table.getValue(1));
		assertEquals(exValue, JANE);
	}
	
	@Test
	public void modifyValueShouldNotChangeTheValueIfItDoesNotExist() {
		table.addValue(1, JANE);
		
		String exValue = table.modifyValue(2, GARRUS);

		assertEquals(JANE, table.getValue(1));
		assertNotEquals(GARRUS, table.getValue(2));
		assertNull(exValue);
	}
	
	@Test
	public void removeValueShouldRemoveTheValueIfItExists() {
		table.addValue(1, JANE);
		table.addValue(2, GARRUS);
		
		String exValue = table.removeValue(2);

		assertEquals(JANE, table.getValue(1));
		assertEquals(exValue, GARRUS);
		assertNull(table.getValue(2));
	}
	
	@Test
	public void removeValueShouldNotRemoveTheValueIfItDoesNotExist() {
		table.addValue(1, JANE);
		
		String exValue = table.modifyValue(2, GARRUS);

		assertEquals(JANE, table.getValue(1));
		assertNotEquals(GARRUS, table.getValue(2));
		assertNull(exValue);
	}
	
	@Test
	public void siteShouldReturn0IfTableIsEmpty() {
		assertEquals(0, table.size());
	}
	
	@Test
	public void siteShouldReturnTheSize() {
		table.addValue(1, JANE);
		table.addValue(2, GARRUS);
		
		assertEquals(2, table.size());
	}
	
	@Test
	public void iteratorShouldIterateOverTheKeysOfTable() {
		table.addValue(1, JANE);
		table.addValue(2, GARRUS);
		table.addValue(3, "Miranda LAWSON");
		
		Iterator<Integer> iterator = table.iterator();
		
		Integer key = iterator.next();
		
		assertEquals(JANE, table.getValue(key));
		
		key = iterator.next();
		
		assertEquals(GARRUS, table.getValue(key));
		
		key = iterator.next();
		
		assertEquals("Miranda LAWSON", table.getValue(key));
	}
	
	@Test
	public void iteratorHasNextShouldSayFalseOnlyAtTheEnd() {
		table.addValue(1, JANE);
		table.addValue(2, GARRUS);
		table.addValue(3, "Miranda LAWSON");
		
		Iterator<Integer> iterator = table.iterator();
		
		assertEquals(true, iterator.hasNext());
		iterator.next();
		assertEquals(true, iterator.hasNext());
		iterator.next();
		assertEquals(true, iterator.hasNext());
		iterator.next();
		assertEquals(false, iterator.hasNext());
	}
	
	@Test
	public void iteratorRemoveShouldRemoveTheKeyValuePair() {
		table.addValue(1, JANE);
		table.addValue(2, GARRUS);
		table.addValue(3, "Miranda LAWSON");
		
		Iterator<Integer> iterator = table.iterator();

		assertEquals(true, iterator.hasNext());
		Integer key = iterator.next();
		assertEquals(JANE, table.getValue(key));

		assertEquals(true, iterator.hasNext());
		key = iterator.next();
		assertEquals(GARRUS, table.getValue(key));
		iterator.remove();
		
		assertEquals(true, iterator.hasNext());
		key = iterator.next();
		assertEquals("Miranda LAWSON", table.getValue(key));
		
		assertEquals(false, iterator.hasNext());
		
		assertEquals(JANE, table.getValue(1));
		assertEquals("Miranda LAWSON", table.getValue(3));
		assertNull(table.getValue(2));
	}
}
