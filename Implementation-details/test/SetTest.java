package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import impl.Set;
import interfaces.Iterator;

class SetTest {
	
	impl.Set<Integer> set = new impl.Set<Integer>(Integer.class);
	
	@Test
	public void testAddObject() {
		assertEquals(0, set.size());

		set.add(1);

		assertEquals(1, set.size());
	}
	
	@Test
	public void testAddShouldAddAnAlreadyExistingValue() {
		assertEquals(0, set.size());

		set.add(1);

		assertEquals(1, set.size());
		
		set.add(1);
		
		assertEquals(1, set.size());
	}
	
	@Test
	public void toArrayShouldReturnAnArrayWithTheExpectedElements() {
		int[] expectedArray = { 1, 2, 3 };
		
		set.add(1);
		set.add(2);
		set.add(3);
		
		for (int i = 0; i < 3; i ++) {
			assertEquals(expectedArray[i], set.toArray()[i]);
		}
	}
	
	@Test
	public void clearShouldClearTheSet() {
		set.add(1);
		set.add(2);
		
		assertEquals(2, set.size());
		
		set.clear();
		
		assertEquals(0, set.size());
	}

	@Test
	public void cloneShouldReturnACopyOfTheSet() {
		set.add(1);
		set.add(2);
		
		impl.Set<Integer> cloneSet = set.clone();

		assertEquals(2, set.size());
		assertEquals(2, cloneSet.size());
		
		cloneSet.add(3);
		
		assertEquals(2, set.size());
		assertEquals(3, cloneSet.size());
		
		set.clear();
		
		assertEquals(0, set.size());
		assertEquals(3, cloneSet.size());
	}
	
	@Test
	public void containsShouldReturnFalseIfObjectIsNotInTheSet() {
		set.add(1);

		assertEquals(false, set.contains(2));
	}
	
	@Test
	public void containsShouldReturnTrueIfObjectIsInTheSet() {
		set.add(1);

		assertEquals(true, set.contains(1));
	}
	
	@Test
	public void isEmptyShouldFalseIfSetIsNotEmpty() {
		set.add(1);

		assertEquals(false, set.isEmpty());
	}
	
	@Test
	public void isEmptyShouldTrueIfSetIsEmpty() {
		assertEquals(true, set.isEmpty());
	}
	
	@Test
	public void shouldRemoveExistingElement() {
		set.add(1);
		assertEquals(1, set.size());
		
		boolean removed = set.remove(1);

		assertEquals(0, set.size());
		assertEquals(true, removed);
	}
	
	@Test
	public void shouldNotRemoveNonExistingElement() {
		set.add(1);
		assertEquals(1, set.size());
		
		boolean removed = set.remove(2);

		assertEquals(1, set.size());
		assertEquals(false, removed);
	}
	
	@Test
	public void equalsShouldReturnFalseIfSetsAreNotEquals() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		setToCompare.add(1);
		set.add(1);
		set.add(2);
		
		assertEquals(false, set.equals(setToCompare));
	}
	
	@Test
	public void equalsShouldReturnTrueIfSetsAreEquals() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		setToCompare.add(1);
		set.add(1);
		
		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void includedShouldReturnTrueIfSetIsIncludedInOtherSet() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		setToCompare.add(1);
		set.add(1);
		set.add(2);
		
		assertEquals(true, set.included(setToCompare));
	}
	
	@Test
	public void includedShouldReturnTrueIfSetsAreEquals() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		setToCompare.add(1);
		set.add(1);
		
		assertEquals(true, set.included(setToCompare));
	}
	
	@Test
	public void includedShouldReturnFalseIfSetIsNotIncludedBecauseValuesAreDifferent() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		setToCompare.add(1);
		setToCompare.add(3);
		set.add(1);
		set.add(2);
		
		assertEquals(false, set.included(setToCompare));
	}
	
	@Test
	public void includedShouldReturnFalseIfSetIsNotIncludedBecauseSetToCompareIsBigger() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		setToCompare.add(1);
		setToCompare.add(2);
		setToCompare.add(3);
		set.add(1);
		set.add(2);
		
		assertEquals(false, set.included(setToCompare));
	}
	
	@Test
	public void includedShouldReturnTrueIfSetIsNotIncludedBecauseSetToCompareIsEmpty() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);

		set.add(1);
		set.add(2);
		
		assertEquals(true, set.included(setToCompare));
	}
	
	@Test
	public void disjointShouldReturnFalseIfBothSetsAreEmpty() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		assertEquals(true, set.disjoint(setToCompare));
	}
	
	@Test
	public void disjointShouldReturnFalseIfSetIsEmpty() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		setToCompare.add(1);
		
		assertEquals(true, set.disjoint(setToCompare));
	}
	
	@Test
	public void disjointShouldReturnFalseIfSetToCompareIsEmpty() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		
		assertEquals(true, set.disjoint(setToCompare));
	}
	
	@Test
	public void disjointShouldReturnTrueIfBothSetsAreDisjoints() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToCompare.add(3);
		setToCompare.add(4);

		assertEquals(true, set.disjoint(setToCompare));
	}
	
	@Test
	public void disjointShouldReturnFalseIfBothIfTheyHaveOneValueInCommon() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(3);
		setToCompare.add(3);
		setToCompare.add(4);

		assertEquals(false, set.disjoint(setToCompare));
	}
	
	@Test
	public void disjointShouldReturnFalseIfBothIfTheyAreEqual() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToCompare.add(1);
		setToCompare.add(2);

		assertEquals(false, set.disjoint(setToCompare));
	}
	
	@Test
	public void unionShouldAddAllTheElementsNominalCase() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToAdd = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToAdd.add(3);
		setToAdd.add(4);
		setToCompare.add(1);
		setToCompare.add(2);
		setToCompare.add(3);
		setToCompare.add(4);

		set.union(setToAdd);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void unionShouldAddAllTheElementsExceptAlreadyExistingElements() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToAdd = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToAdd.add(2);
		setToAdd.add(3);
		setToCompare.add(1);
		setToCompare.add(2);
		setToCompare.add(3);

		set.union(setToAdd);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void unionShouldAddNoElementIfSetToAddIsIncludedInSet() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToAdd = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToAdd.add(2);
		setToAdd.add(1);
		setToCompare.add(1);
		setToCompare.add(2);

		set.union(setToAdd);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void unionShouldAddAllElementsToEmptySet() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToAdd = new impl.Set<Integer>(Integer.class);

		setToAdd.add(2);
		setToAdd.add(1);
		setToCompare.add(1);
		setToCompare.add(2);

		set.union(setToAdd);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void unionShouldAddNoElementIfEmptySetIsAdded() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToAdd = new impl.Set<Integer>(Integer.class);

		set.add(2);
		set.add(1);
		setToCompare.add(1);
		setToCompare.add(2);

		set.union(setToAdd);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void differenceShouldRemoveElements() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToRemove.add(2);
		setToRemove.add(3);
		setToCompare.add(1);

		set.difference(setToRemove);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void differenceShouldMakeEmptySetIfAllElementsAreInSet() {
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToRemove.add(1);
		setToRemove.add(2);

		set.difference(setToRemove);

		assertEquals(true, set.isEmpty());
	}
	
	@Test
	public void differenceShouldRemoveNoElementIfNoneOfTheElementsAreInSet() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToRemove.add(4);
		setToRemove.add(3);
		setToCompare.add(1);
		setToCompare.add(2);

		set.difference(setToRemove);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void differenceShouldRemoveNoElementsIfParameterIsEmptySet() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);

		set.add(2);
		set.add(1);
		setToCompare.add(1);
		setToCompare.add(2);

		set.difference(setToRemove);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void differenceShouldStillBeEmptyIfItWasEmptyBefore() {
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);

		setToRemove.add(1);
		setToRemove.add(2);

		set.difference(setToRemove);

		assertEquals(true, set.isEmpty());
	}
	
	@Test
	public void intersectionShouldRemoveElements() {
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToRemove.add(2);
		setToRemove.add(3);
		setToCompare.add(2);

		set.intersection(setToRemove);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void intersectionShouldRemoveNoElementIfAllElementsAreInSet() {
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToRemove.add(1);
		setToRemove.add(2);
		setToCompare.add(1);
		setToCompare.add(2);

		set.intersection(setToRemove);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void intersectionShouldRemoveAllElementsIfNoneOfTheElementsAreInSet() {
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);
		
		set.add(1);
		set.add(2);
		setToRemove.add(4);
		setToRemove.add(3);

		set.intersection(setToRemove);

		assertEquals(true, set.isEmpty());
	}
	
	@Test
	public void intersectionShouldLeaveAnEmptySetIfParameterIsEmptySet() {
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);

		set.add(2);
		set.add(1);

		set.intersection(setToRemove);

		assertEquals(true, set.isEmpty());
	}
	
	@Test
	public void intersectionShouldStillBeEmptyIfItWasEmptyBefore() {
		impl.Set<Integer> setToRemove = new impl.Set<Integer>(Integer.class);

		setToRemove.add(1);
		setToRemove.add(2);

		set.intersection(setToRemove);

		assertEquals(true, set.isEmpty());
	}
	
	@Test
	public void symmetricDifferenceNominalCase() {
		impl.Set<Integer> paramSet = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);
		
		set.add(2);
		set.add(5);
		paramSet.add(1);
		paramSet.add(2);

		setToCompare.add(5);
		setToCompare.add(1);

		set.symmetricDifference(paramSet);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void symmetricDifferenceShouldReturnTheEquivalentOfParamSetIfSetIsEmpty() {
		impl.Set<Integer> paramSet = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);

		paramSet.add(1);
		paramSet.add(2);

		setToCompare.add(2);
		setToCompare.add(1);

		set.symmetricDifference(paramSet);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void symmetricDifferenceShouldReturnTheEquivalentOfSetIfParamSetIsEmpty() {
		impl.Set<Integer> paramSet = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);

		set.add(1);
		set.add(2);

		setToCompare.add(2);
		setToCompare.add(1);

		set.symmetricDifference(paramSet);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void symmetricDifferenceShouldReturnAnEmptySetIfBothSetsAreEqual() {
		impl.Set<Integer> paramSet = new impl.Set<Integer>(Integer.class);

		set.add(1);
		set.add(2);
		paramSet.add(1);
		paramSet.add(2);

		set.symmetricDifference(paramSet);

		assertEquals(true, set.isEmpty());
	}
	
	@Test
	public void symmetricDifferenceShouldReturnAllElementsIfNoneAreCommonToBothSets() {
		impl.Set<Integer> paramSet = new impl.Set<Integer>(Integer.class);
		impl.Set<Integer> setToCompare = new impl.Set<Integer>(Integer.class);

		set.add(1);
		set.add(2);
		paramSet.add(3);
		paramSet.add(4);
		
		setToCompare.add(2);
		setToCompare.add(1);
		setToCompare.add(3);
		setToCompare.add(4);

		set.symmetricDifference(paramSet);

		assertEquals(true, set.equals(setToCompare));
	}
	
	@Test
	public void iteratorShouldIterateOverTheValues() {
		set.add(1);
		set.add(2);
		set.add(3);
		
		Iterator<Integer> iterator = set.iterator();
		
		Integer key = iterator.next();
		
		assertEquals(1, key);
		
		key = iterator.next();
		
		assertEquals(2, key);
		
		key = iterator.next();
		
		assertEquals(3, key);
	}
	
	@Test
	public void iteratorHasNextShouldSayFalseOnlyAtTheEnd() {
		set.add(1);
		set.add(2);
		set.add(3);
		
		Iterator<Integer> iterator = set.iterator();
		
		assertEquals(true, iterator.hasNext());
		iterator.next();
		assertEquals(true, iterator.hasNext());
		iterator.next();
		assertEquals(true, iterator.hasNext());
		iterator.next();
		assertEquals(false, iterator.hasNext());
	}
	
	@Test
	public void iteratorRemoveShouldRemoveTheValue() {
		set.add(1);
		set.add(2);
		set.add(3);
		
		Iterator<Integer> iterator = set.iterator();

		assertEquals(true, iterator.hasNext());
		Integer key = iterator.next();
		assertEquals(1, key);

		assertEquals(true, iterator.hasNext());
		key = iterator.next();
		assertEquals(2, key);
		iterator.remove();
		
		assertEquals(true, iterator.hasNext());
		key = iterator.next();
		assertEquals(3, key);
		
		assertEquals(false, iterator.hasNext());
		
		assertEquals(true, set.contains(1));
		assertEquals(true, set.contains(3));
		assertEquals(false, set.contains(2));
	}
}
