package interfaces;

public interface ITable<K, V> {
    void clear();
    boolean contains(K key);
    V getValue(K key);
    boolean isEmpty();
    Iterator<K> iterator();
    void addValue(K key, V value);
    V modifyValue(K key, V value);
    V removeValue(K key);
    int size();
}
