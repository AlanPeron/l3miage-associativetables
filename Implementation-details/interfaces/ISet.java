package interfaces;


public interface ISet<T> {
	boolean add(T element);
	void clear();
	ISet<T> clone();
	boolean contains(T element);
	boolean isEmpty();
	public Iterator<T> iterator();
	boolean remove(T element);
	int size();
	T[] toArray();
	boolean equals(ISet<T> set);
	boolean included(ISet<T> set);
	boolean disjoint(ISet<T> set);
	void union(ISet<T> set);
	void intersection(ISet<T> set);
	void difference(ISet<T> set);
	void symmetricDifference(ISet<T> set);
}
