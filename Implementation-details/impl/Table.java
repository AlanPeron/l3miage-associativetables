package impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import interfaces.Iterator;
import interfaces.ITable;

public class Table<K, V> implements ITable<K, V> {
	private Map<K, V> internalMap = new HashMap<K,V>();

	@Override
	public void clear() {
		internalMap.clear();
	}

	@Override
	public boolean contains(K key) {
		return internalMap.containsKey(key);
	}

	@Override
	public V getValue(K key) {
		return internalMap.get(key);
	}

	@Override
	public boolean isEmpty() {
		return internalMap.isEmpty();
	}

	@Override
	public Iterator<K> iterator() {
		return new InternalIterator<K>();
	}

	@Override
	public void addValue(K key, V value) {
		internalMap.putIfAbsent(key, value);
	}

	@Override
	public V modifyValue(K key, V value) {
		return internalMap.replace(key, value);
	}

	@Override
	public V removeValue(K key) {
		return internalMap.remove(key);
	}

	@Override
	public int size() {
		return internalMap.size();
	}
	
	public String toString() {
		String resultString = "\n";

		for (var entry : internalMap.entrySet()) {
			String displayValueString = entry.getValue().toString();

			if (entry.getValue() instanceof Table<?, ?>) {
				//Add an additional level of indentation for Tables that are values of this table
				displayValueString = displayValueString.replaceAll("\n", "\n     ");
			}

			resultString = resultString + entry.getKey().toString() + " -> " + displayValueString + "\n";
		}
		
		return resultString;
	}

	private class InternalIterator<K> implements Iterator<K> {
		int index = -1;
		K[] keys = (K[]) internalMap.keySet().toArray();

		@Override
		public boolean hasNext() {
			return index != keys.length - 1;
		}

		@Override
		public K next() {
			index = index + 1;
			return keys[index];
		}

		@Override
		public void remove() {
			internalMap.remove(keys[index]);
			index--;
			keys = (K[]) internalMap.keySet().toArray();
		}
	}
}
