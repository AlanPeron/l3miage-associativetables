package impl;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import interfaces.Iterator;
import interfaces.ISet;

public class Set<T> implements ISet<T> {
	private List<T> listInternal = new ArrayList<T>();
	private Class classInternal = null;
	
	public Set(Class classInternal) {
		this.classInternal = classInternal;
	}

	@Override
	public boolean add(T element) {
		if (!listInternal.contains(element)) {
			listInternal.add(element);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void clear() {
		this.listInternal = new ArrayList<>();
	}

	@Override
	public Set<T> clone() {
		Set<T> cloneSet = new Set<>(classInternal);
		
		for (int j = 0; j < listInternal.size(); j++) {
			cloneSet.add(listInternal.get(j));
		}

		return cloneSet;
	}

	@Override
	public boolean contains(T element) {
		return listInternal.contains(element);
	}

	@Override
	public boolean isEmpty() {
		return listInternal.size() == 0;
	}

	@Override
	public Iterator<T> iterator() {
		return new InternalIterator<>();
	}

	@Override
	public boolean remove(T element) {
		return listInternal.remove(element);
	}

	@Override
	public int size() {
		return listInternal.size();
	}

	@Override
	public T[] toArray() {
		T[] array = (T[]) Array.newInstance(classInternal, listInternal.size());
		
		for (int j = 0; j < array.length; j++) {
			array[j] =  listInternal.get(j);
		}

		return array;
	}

	@Override
	public boolean equals(ISet<T> set) {
		return set.size() == listInternal.size() && this.included(set);
	}

	@Override
	public boolean included(ISet<T> set) {
		T[] setArray = set.toArray();

		for (int j = 0; j < set.size(); j++) {
			if(!listInternal.contains(setArray[j])) return false;
		}

		return true;
	}

	@Override
	public boolean disjoint(ISet<T> set) {
		T[] setArray = set.toArray();

		for (int j = 0; j < set.size(); j++) {
			if(listInternal.contains(setArray[j])) return false;
		}

		return true;
	}

	@Override
	public void union(ISet<T> set) {
		T[] setArray = set.toArray();

		for (int j = 0; j < set.size(); j++) {
			this.add(setArray[j]);
		}
	}

	@Override
	public void intersection(ISet<T> set) {
		T[] setArray = this.toArray();

		for (int j = 0; j < setArray.length; j++) {
			if(!set.contains(setArray[j])) this.remove(setArray[j]);
		}
	}

	@Override
	public void difference(ISet<T> set) {
		T[] setArray = set.toArray();

		for (int j = 0; j < set.size(); j++) {
			this.remove(setArray[j]);
		}
	}

	@Override
	public void symmetricDifference(ISet<T> set) {
		ISet<T> clone = this.clone();
		clone.intersection(set);

		this.union(set);
		this.difference(clone);
	}
	
	public String toString() {
		if (this.size() == 0) return "[]";

		String resultString = "[ ";

		for (var elem : listInternal) {
			resultString = resultString + elem.toString() + ", ";
		}
		
		return resultString.substring(0, resultString.length() - 2) + " ]";
	}
	
	private class InternalIterator<K> implements Iterator<K> {
		int index = -1;
		K[] keys = (K[]) listInternal.toArray();

		@Override
		public boolean hasNext() {
			return index != keys.length - 1;
		}

		@Override
		public K next() {
			index = index + 1;
			return keys[index];
		}

		@Override
		public void remove() {
			listInternal.remove(keys[index]);
			index--;
			keys = (K[]) listInternal.toArray();
		}
	}
	
}
