Implementation of the Set/associative tables interface given in the l3 miage during the 2022/2023 year.
The goal is to have something of substance to train instead of trying to do it on f*cking paper in the goddam year 2023.

To import the projet in eclipse:
`File -> Import`

In the popup, select:
`Git -> Projects from git -> Clone URI`

In URI, copy/paste :
`https://gitlab.com/AlanPeron/l3miage-associativetables.git`

Then `next` -> select `master` -> `next`

In `local Destination` -> `Directory` -> select the workspace and add to the end of the path the name you wish to give to the project:
ex: `C:\Users\User\Documents\workspaceJava\MIAGEL3-AssociativeTables`

Then click `next`

Select `Import existing eclipse project` -> `next` -> `finish`

Structure of the project:
Implementation-details: Contains everything that is required to get a functional class that have the same function calls as the Set/Table/iterator given during the 2022/2023 school year to L3 MIAGE student. Except for bug fixes, this should not be touched.

Src -> main contains what is likely to change from exercice to exercise.
